package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
	)

func TestPoint_LatRad(t *testing.T) {
	tests := map[string]struct{
		point Point
		expected float64
	}{
		"1": {Point{1, 0}, 0.0174533},
		"0": {Point{0, 0}, 0.0},
	}
	for k,v := range tests{
		t.Run(k, func(t *testing.T) {
			assert.InDelta(t, v.expected, v.point.LatRad(), 0.000001)
		})
	}
}


func TestPoint_LonRad(t *testing.T) {
	tests := map[string]struct{
		point Point
		expected float64
	}{
		"1": {Point{0, 1}, 0.0174533},
		"0": {Point{0, 0}, 0.0},
	}
	for k,v := range tests{
		t.Run(k, func(t *testing.T) {
			assert.InDelta(t, v.expected, v.point.LonRad(), 0.000001)
		})
	}
}

func TestPoint_DistanceTo(t *testing.T) {
	tests := map[string]struct{
		a,b Point
		expected, delta float64
	}{
		"S-N": {
			Point{89.999999,0.000000},
			Point{-89.999999,0.000000},
			20021.37, 10},
		"S-MID": {
			Point{89.999999,0.000000},
			Point{0,0.0},
			10010.68, 10},
		"N-MID": {Point{-89.999999,0.000000},
			Point{0,0.0},
			10010.68, 10},
		}
	for k,v := range tests{
		t.Run(k, func(t *testing.T) {
			assert.InDelta(t, v.expected, v.a.DistanceTo(v.b), v.delta)
		})
	}
}