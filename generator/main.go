package main

import (
		"flag"
	"math/rand"
	"time"
	"encoding/csv"
	"os"
	"log"
	"strconv"
)

var (
	rowNum int
	baseLat, baseLon, delta float64
	outputFilename string
)
func init() {

	flag.IntVar(&rowNum, "n", 100, "Row number to generate")
	flag.Float64Var(&baseLat, "baseLat", 51.925146, "Base Latitude")
	flag.Float64Var(&baseLon, "baseLon", 4.478617, "Base Longitude")
	flag.Float64Var(&delta, "delta", 10.0, "Delta")
	flag.StringVar(&outputFilename, "output", "output.csv", "Output filename")
	flag.Parse()
}
func main() {
	LatDeltaAbs := delta
	LonDeltaAbs := delta

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	file, err := os.Create(outputFilename)

	if err != nil {
		log.Fatalln(err)
	}

	defer file.Close()

	outCsv := csv.NewWriter(file)

	outCsv.Write([]string{`id`,`lat`,`lon`})

	tick := time.Tick(1 * time.Second)
	var i int
	report := func () {
		stat, _ := file.Stat()
		log.Printf("%d/%d | %.2f%% | %.2fMiB\n", i, rowNum, float64(i)/ float64(rowNum) * 100, float64(stat.Size()) / 1024.0 / 1024.0)
	}

	go func() {
		for {
			<-tick
			report()
		}
	}()

	started := time.Now()
	for i = 0; i < rowNum; i++ {
		id := r.Int63()
		dlat := LatDeltaAbs * r.Float64()
		if r.Float64() > 0.5 {
			dlat = -dlat
		}
		dlon := LonDeltaAbs * r.Float64()
		if r.Float64() > 0.5 {
			dlon = -dlon
		}
		lat := baseLat + dlat
		lon := baseLon + dlon
		outCsv.Write([]string{
			strconv.FormatInt(id, 10),
			strconv.FormatFloat(lat, 'f', 5, 64),
			strconv.FormatFloat(lon, 'f', 5, 64)})
	}
	ended := time.Now()
	outCsv.Flush()
	report()
	log.Printf("Done in %s\n", ended.Sub(started))
}
