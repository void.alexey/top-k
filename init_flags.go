//+build !test

package main

import (
	"flag"
	"os"
)

func init() {
	flag.StringVar(&inputFile, "inputFile", "", "Input CSV")
	flag.IntVar(&skipLines, "skipLines", 0, "Skip lines from CSV")
	flag.IntVar(&mapCount, "mapCount", 1, "Map worker count")
	flag.Float64Var(&baseLat, "baseLat", 51.925146, "Base latitude")
	flag.Float64Var(&baseLon, "baseLon", 4.478617, "Base longitude")
	flag.Parse()
	if inputFile == ""  || mapCount <= 0 {
		flag.Usage()
		os.Exit(1)
	}
	basePoint = Point{baseLat, baseLon}
}
