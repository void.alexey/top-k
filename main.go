package main

import (
	"os"
	"log"
	"encoding/csv"
	"context"
	"os/signal"
	"fmt"
	"sync"
	"time"
)

var (
	inputFile string
	mapCount int
	baseLat, baseLon float64
	basePoint Point
	skipLines int
)

func MergeErrors(cs ...<-chan error) <-chan error {
	var wg sync.WaitGroup
	out := make(chan error, len(cs))

	output := func(c <-chan error) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func WaitError(errs ...<-chan error) error {
	errc := MergeErrors(errs...)
	for err := range errc {
		if err != nil {
			return err
		}
	}
	return nil
}

func Process(ctx context.Context, reader CsvReader, jobCount int) ([]Record, []Record, error) {
	errcChList := make([]<-chan error, 0)

	streamCh, streamErrCh, err := StreamCsvReader(ctx, reader)
	if err != nil {
		return nil, nil, err
	}
	errcChList = append(errcChList, streamErrCh)

	calculatedChList := make([]<-chan Record, 0)

	for i :=0; i < jobCount; i ++ {
		calculatedCh, parseErrCh := ParseMapper(ctx, streamCh)
		calculatedChList = append(calculatedChList, calculatedCh)
		errcChList = append(errcChList, parseErrCh)
	}

	calculatedBcastList := FanOutRecords(FanInRecords(calculatedChList...), 2)

	closestCh := Top5ClosestReducer(ctx, calculatedBcastList[0])
	farthestCh := Top5FarthestReducer(ctx, calculatedBcastList[1])

	if err := WaitError(errcChList...); err != nil {
		return nil, nil, err
	}

	return <-closestCh, <-farthestCh, nil
}


func Printout(in []Record) {
	for k, v := range in {
		fmt.Printf("%d: %+v\n", k, v)
	}
}

func main() {
	taskCtx, cancelTaskCtx := context.WithCancel(context.Background())

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	go func() {
		sig := <-stop
		log.Println("Exit by signal:", sig)
		cancelTaskCtx()
		os.Exit(2)
	}()


	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalln(err)
	}

	csvReader := csv.NewReader(file)
	csvReader.FieldsPerRecord = 3

	for i := 0; i < skipLines; i++ {
		_, err := csvReader.Read()
		if err != nil {
			log.Fatalln("reader error on skip", err)
		}
	}

	started := time.Now()
	closest, farthest, err := Process(taskCtx, csvReader, mapCount)
	log.Printf("Map count: %d\n", mapCount)
	log.Printf("Done in %v\n", time.Now().Sub(started))
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("Closest:")
	Printout(closest)
	log.Println("Farthest:")
	Printout(farthest)

}
