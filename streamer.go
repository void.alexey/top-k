package main

import (
		"io"
	"context"
	"errors"
	)

var (
	ErrStreamerNoReader = errors.New("noReader")
)

type CsvReader interface {
	Read() ([]string, error)
}
func StreamCsvReader(ctx context.Context, reader CsvReader) (<-chan []string, <-chan error, error) {
	out := make(chan []string)
	errc := make(chan error, 1)

	if reader == nil {
		return nil, nil, ErrStreamerNoReader
	}

	go func() {
		defer close(out)
		defer close(errc)
		for {
			line, err := reader.Read()
			if err == io.EOF {
				return
			}
			if err != nil {
				errc <- err
				return
			}
			select {
			case out <- line:
				continue
			case <- ctx.Done():
				return
			}
		}
	}()
	return out, errc, nil
}
