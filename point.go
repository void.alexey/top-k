package main

import "math"

const (
	EarthRadius = 6371
)

type Point struct {
	Lat, Lon float64
}

func deg2rad(d float64) (float64) {
	return d * (math.Pi / 180)
}

func (p *Point) LatRad() float64 {
	return deg2rad(p.Lat)
}

func (p *Point) LonRad() float64 {
	return deg2rad(p.Lon)
}

func (p *Point) DistanceTo(p2 Point) float64 {
	dLat := deg2rad(p.Lat - p2.Lat)
	dLon := deg2rad(p.Lon - p2.Lon)

	a1 := math.Sin(dLat/2) * math.Sin(dLat/2)
	a2 := math.Sin(dLon/2) * math.Sin(dLon/2) * math.Cos(p.LatRad()) * math.Cos(p2.LatRad())

	a := a1 + a2
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return EarthRadius * c
}

