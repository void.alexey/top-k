
usage:
	@echo "Usage: make build|test|test-short|vendor|dep"

.PHONY: dep
dep:
	dep ensure -vendor-only -v

vendor:
	dep ensure -vendor-only -v

test: vendor
	go test -v -tags test .

test-short: vendor
	go test -v -short -tags test .

build:
	go build .