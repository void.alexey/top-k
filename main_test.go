package main

import (
	"testing"
	"encoding/csv"
	"os"
	"github.com/stretchr/testify/assert"
	"context"
)


func init() {
	basePoint = Point{51.925146, 4.478617}
}

func TestProcess(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		file, err := os.Open("testdata/geoData.csv")
		assert.NoError(t, err)

		inputCsv := csv.NewReader(file)
		inputCsv.Read()

		closest, farthest, err := Process(context.Background(), inputCsv, 1)
		assert.NoError(t, err)
		assert.Equal(t, []Record{
			{Id:"442406", Point:Point{Lat:51.9271671, Lon:4.4822171}, Distance:0.3338381556847423},
			{Id:"285782", Point:Point{Lat:51.9253559, Lon:4.4863098}, Distance:0.5280320575225937},
			{Id:"429151", Point:Point{Lat:51.92562969999999, Lon:4.4880344}, Distance:0.6480104060043963},
			{Id:"512818", Point:Point{Lat:51.9268152, Lon:4.489072}, Distance:0.7405525423045783},
			{Id:"25182", Point:Point{Lat:51.92491219999999, Lon:4.490593}, Distance:0.8216419667395297}}, closest)
		assert.Equal(t, []Record{
			{Id:"7818", Point:Point{Lat:37.8667538, Lon:-122.25909899999999}, Distance:8776.646278220525},
			{Id:"382013", Point:Point{Lat:37.39945, Lon:-5.971514}, Distance:1810.117975607402},
			{Id:"381823", Point:Point{Lat:37.168004, Lon:-3.602987}, Distance:1758.8482704875876},
			{Id:"382582", Point:Point{Lat:37.1768672, Lon:-3.608897}, Distance:1758.0806131200134},
			{Id:"382693", Point:Point{Lat:40.9702399, Lon:-5.661052}, Distance:1441.7191536957162}}, farthest)
	})

	t.Run("stream no reader", func(t *testing.T) {
		_, _, err := Process(context.Background(), nil, 1)
		assert.EqualError(t, err, ErrStreamerNoReader.Error())
	})

	t.Run("stream read err", func(t *testing.T) {
		file, err := os.Open("testdata/geoData_reader_err.csv")
		assert.NoError(t, err)

		inputCsv := csv.NewReader(file)
		inputCsv.Read()

		_, _, err = Process(context.Background(), inputCsv, 1)
		assert.IsType(t, &csv.ParseError{}, err)
	})

	t.Run("parse err", func(t *testing.T) {
		file, err := os.Open("testdata/geoData_broken_lat.csv")
		assert.NoError(t, err)

		inputCsv := csv.NewReader(file)
		inputCsv.Read()

		_, _, err = Process(context.Background(), inputCsv, 1)
		if assert.IsType(t, ParserError{}, err) {
			assert.Equal(t, ErrParseLat, err.(ParserError).Message)
		}
	})

}