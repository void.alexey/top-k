package main

import (
	"testing"
	"context"
	"github.com/stretchr/testify/assert"
	"errors"
	"time"
)

func TestParseMapper(t *testing.T) {
	t.Run("min field count error", func (t *testing.T) {
		inCh := make(chan []string)
		_, errCh := ParseMapper(context.Background(), inCh)
		inCh <- []string{}
		err := <- errCh

		if assert.IsType(t, ParserError{}, err) {
			assert.Equal(t, ErrParseFieldCount, err.(ParserError).Message)
		}
	})

	t.Run("lat parse error", func (t *testing.T) {
		inCh := make(chan []string)
		_, errCh := ParseMapper(context.Background(), inCh)
		inCh <- []string{"id", "aaa", "0.0"}
		err := <- errCh

		if assert.IsType(t, ParserError{}, err) {
			assert.Equal(t, ErrParseLat, err.(ParserError).Message)
		}
	})

	t.Run("lon parse error", func (t *testing.T) {
		inCh := make(chan []string)
		_, errCh := ParseMapper(context.Background(), inCh)
		inCh <- []string{"id", "0.0", "aaa"}
		err := <- errCh

		if assert.IsType(t, ParserError{}, err) {
			assert.Equal(t, ErrParseLon, err.(ParserError).Message)
		}
	})

	t.Run("successful", func (t *testing.T) {
		inCh := make(chan []string)
		outCh, _ := ParseMapper(context.Background(), inCh)
		inCh <- []string{"id", "0.0", "0.0"}
		rec := <- outCh
		assert.Equal(t, Record{"id", Point{0.0,0.0},5789.038318681406}, rec)
	})

	t.Run("context done", func (t *testing.T) {
		if testing.Short() {
			t.Skip("skip context wait tests in short mode")
		}
		ctx, cancelCtx := context.WithCancel(context.Background())
		cancelCtx()

		inCh := make(chan []string)
		outCh, _ := ParseMapper(ctx, inCh)
		inCh <- []string{"id", "0.0", "0.0"}

		time.Sleep(1 * time.Second)
		_, ok := <- outCh
		assert.False(t, ok)
	})
}

func TestFanInRecords(t *testing.T) {
	ch1 := make(chan Record)
	ch2 := make(chan Record)
	outCh := FanInRecords(ch1, ch2)

	rec1 := Record{}
	rec2 := Record{}
	ch1 <- rec1
	assert.Equal(t, rec1, <- outCh)
	ch2 <- rec2
	assert.Equal(t, rec1, <- outCh)
	assert.Equal(t, 0, len(outCh))

	close(ch1)
	close(ch2)
	_, ok := <- outCh
	assert.False(t, ok)
}

func TestFanOutRecords(t *testing.T) {
	rec := Record{}
	inCh := make(chan Record)
	outChList := FanOutRecords(inCh, 2)
	inCh <- rec

	assert.Equal(t, rec, <- outChList[0])
	assert.Equal(t, rec, <- outChList[1])

	close(inCh)

	select {
	case _, ok := <- outChList[0]:
		assert.False(t, ok)
	case _, ok := <- outChList[1]:
		assert.False(t, ok)
	case <-time.After(2 * time.Second):
		t.Error("close wait timeout")
	}
}

func TestTop5ClosestReducer(t *testing.T) {
	inCh := make(chan Record)
	outCh := Top5ClosestReducer(context.Background(), inCh)
	inCh <- Record{Distance: 100.0}
	inCh <- Record{Distance: 50.0}
	inCh <- Record{Distance: 60.0}
	inCh <- Record{Distance: 40.0}
	inCh <- Record{Distance: 70.0}
	inCh <- Record{Distance: 25.0}
	inCh <- Record{Distance: 95.0}
	close(inCh)
	assert.Equal(t, []Record{{Distance: 25.0},{Distance: 40.0},{Distance: 50.0},{Distance: 60.0},{Distance: 70.0}}, <- outCh)
}


func TestTop5FarthestReducer(t *testing.T) {
	inCh := make(chan Record)
	outCh := Top5FarthestReducer(context.Background(), inCh)
	inCh <- Record{Distance: 100.0}
	inCh <- Record{Distance: 50.0}
	inCh <- Record{Distance: 60.0}
	inCh <- Record{Distance: 40.0}
	inCh <- Record{Distance: 70.0}
	inCh <- Record{Distance: 25.0}
	inCh <- Record{Distance: 95.0}
	close(inCh)
	assert.Equal(t, []Record{{Distance: 100.0},{Distance: 95.0},{Distance: 70.0},{Distance: 60.0},{Distance: 50.0}}, <- outCh)
}

func TestReducer_contextDone(t *testing.T) {
	if testing.Short() {
		t.Skip("skip context wait tests in short mode")
	}
	ctx, cancelCtx := context.WithCancel(context.Background())
	cancelCtx()
	inCh := make(chan Record)
	reducer := func(prev []Record, r Record) []Record {
		return prev
	}
	outCh := Reducer(ctx, reducer, inCh)
	inCh <- Record{}
	time.Sleep(1 * time.Second)
	_, ok := <- outCh
	assert.False(t, ok)
}

func TestReducer_contextDone2(t *testing.T) {
	if testing.Short() {
		t.Skip("skip context wait tests in short mode")
	}
	ctx, cancelCtx := context.WithCancel(context.Background())
	cancelCtx()
	inCh := make(chan Record)
	reducer := func(prev []Record, r Record) []Record {
		return prev
	}
	outCh := Reducer(ctx, reducer, inCh)
	close(inCh)
	time.Sleep(1 * time.Second)
	_, ok := <- outCh
	assert.False(t, ok)
}

func TestParserError_Error(t *testing.T) {
	err := ParserError{"msg", []string{"a","b","c"}, errors.New("err")}
	assert.EqualError(t, err, "msg (err) | Input: [a b c]")
}
