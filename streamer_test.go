package main

import (
	"testing"
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"errors"
	"time"
	)

func TestStreamCsvReader_noReader(t *testing.T) {
	_, _, err := StreamCsvReader(context.Background(), nil)
	assert.EqualError(t, err, ErrStreamerNoReader.Error())
}

type mockReader struct {
	mock.Mock
}

func (m *mockReader) Read() ([]string, error) {
	args := m.Called()
	return args.Get(0).([]string), args.Error(1)
}


func TestStreamCsvReader_readRow(t *testing.T) {
	reader := &mockReader{}
	row := []string{"id","0.0","0.0"}
	reader.On("Read").Return(row, nil).Twice()
	outCh, _, err := StreamCsvReader(context.Background(), reader)
	assert.NoError(t, err)
	out := <- outCh
	assert.Equal(t, row, out)
	reader.AssertExpectations(t)
}

func TestStreamCsvReader_readError(t *testing.T) {
	reader := &mockReader{}
	reader.On("Read").Return([]string{}, errors.New("err")).Once()
	outCh, errCh, err := StreamCsvReader(context.Background(), reader)
	assert.NoError(t, err)
	<- outCh
	assert.EqualError(t, <- errCh, "err")

	reader.AssertExpectations(t)
}

func TestStreamCsvReader_readEOF(t *testing.T) {
	reader := &mockReader{}
	reader.On("Read").Return([]string{}, io.EOF).Once()
	outCh, errCh, err := StreamCsvReader(context.Background(), reader)
	assert.NoError(t, err)
	<- outCh
	assert.NoError(t, <- errCh)
	reader.AssertExpectations(t)
}

func TestStreamCsvReader_ctxDone(t *testing.T) {
	if testing.Short() {
		t.Skip("skip context wait tests in short mode")
	}
	reader := &mockReader{}
	reader.On("Read").Return([]string{}, nil)

	ctx, cancelCtx := context.WithCancel(context.Background())
	cancelCtx()

	outCh, _, err := StreamCsvReader(ctx, reader)
	assert.NoError(t, err)

	time.Sleep(1 * time.Second)
	_, ok := <- outCh
	assert.False(t, ok)
}