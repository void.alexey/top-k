package main

import (
	"strconv"
	"context"
	"fmt"
	"sync"
	"sort"
	)

type Record struct {
	Id string
	Point Point
	Distance float64
}

type ParserError struct {
	Message string
	In []string
	Cause error
}

const (
	ErrParseFieldCount = "Wrong field count"
	ErrParseLat = "Failed to parse lat"
	ErrParseLon = "Failed to parse lon"
)

func (e ParserError) Error() string {
	return fmt.Sprintf("%s (%s) | Input: %+v", e.Message, e.Cause.Error(), e.In)
}

func ParseMapper(ctx context.Context, in <-chan []string) (<-chan Record, <-chan error) {
	out := make(chan Record)
	errc := make(chan error, 1)
	go func() {
		defer close(out)
		defer close(errc)
		for line := range in {
			if len(line) < 3 {
				errc <- ParserError{ErrParseFieldCount, line, nil}
				return
			}
			var lat, lon float64
			var err error
			if lat, err = strconv.ParseFloat(line[1], 64); err != nil {
				errc <- ParserError{ErrParseLat,line, err}
				return
			}
			if lon, err = strconv.ParseFloat(line[2], 64); err != nil {
				errc <- ParserError{ErrParseLon,line, err}
				return
			}
			point := Point{lat,lon}
			rec := Record{line[0], point, basePoint.DistanceTo(point)}

			select {
			case <-ctx.Done():
				return
			case out <- rec:
				continue
			}
		}
	}()
	return out, errc
}


func FanInRecords(inputs ...<-chan Record) (<-chan Record){
	var wg sync.WaitGroup
	out := make(chan Record)
	receive := func(c <-chan Record) {
		for v := range c {
			out <- v
		}
		wg.Done()
	}
	wg.Add(len(inputs))
	for _, c := range inputs {
		go receive(c)
	}
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func FanOutRecords(in <-chan Record, n int) ([]chan Record){

	outs := make([]chan Record, n)
	for i := 0; i < n; i++ {
		outs[i] = make(chan Record)
	}
	go func() {
		defer func() {
			for _, c := range outs { close(c) }
		}()
		for v := range in {
			for _, c := range outs {
				c <- v
			}
		}
	}()
	return outs
}

func MakeTopNReduceFunc(comparator func(a,b Record) bool, n int) (func([]Record, Record) []Record) {
	return func(prev []Record, record Record) ([]Record) {
		if len(prev) < n || comparator(record, prev[len(prev)-1]) {
			result := append(prev, record)
			sort.Slice(result[:], func(i, j int) bool {
				return comparator(result[i], result[j])
			})
			if len(result) > n {
				return result[0:n]
			} else {
				return result
			}
		} else {
			return prev
		}
	}
}

func Reducer(ctx context.Context, reducer func([]Record, Record) []Record, in <-chan Record) (<-chan []Record) {
	out := make(chan []Record)
	result := []Record{}[:]

	go func() {
		defer close(out)
		for record := range in {
			result = reducer(result, record)
			select {
			case <- ctx.Done():
				return
			default:
				continue
			}
		}
		select {
		case out <- result:
			return
		case <- ctx.Done():
			return
		}
	}()
	return out
}

func Top5ClosestReducer(ctx context.Context, in <-chan Record) <-chan []Record {
	cmp := func(a, b Record) bool {
		return a.Distance < b.Distance
	}
	return Reducer(ctx, MakeTopNReduceFunc(cmp, 5), in)
}

func Top5FarthestReducer(ctx context.Context, in <-chan Record) <-chan []Record {
	cmp := func(a, b Record) bool {
		return a.Distance > b.Distance
	}
	return Reducer(ctx, MakeTopNReduceFunc(cmp, 5), in)
}
